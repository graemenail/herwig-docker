# 🐳 Herwig Docker

Dockerfile to generate a bootstrap installation of Herwig and it's dependencies.

The image is based on `ubuntu:20.04`.

## 🏗 Building

This Dockerfile can build an image with Python 2 or Python 3. Optionally, it can also build with LaTeX support.

A Python 2 + LaTeX image can is built with:
```shell
docker build --build-arg PY_VER=2 --build-arg LATEX=1 -t hepdock .
```
The resulting image is called `hepdock`.

## 🏃Running

Having built an image, it can be run via:
```shell
docker run -it -v $PWD:$PWD -w $PWD hepdock Herwig --help
```
where `-v $PWD:$PWD -w $PWD` makes the current directory available to the container, as it's working directory.

It may be useful to define a wrapper for this command:
```shell
function hepdock {
  docker run -it --rm -v $PWD:$PWD -w $PWD hepdock "$@"
}
```
In doing so, it's possible to prefix the usual Herwig commands with `hepdock ` and run them as normal.
```shell
hepdock Herwig --help
```

### LHAPDF
Pre-installed LHAPDF PDFs reside within the docker container. By default running `lhapdf install` would install PDFs inside the containers in a non-persistent way.

The script `env.sh` intercepts such requests and attempts to install them on the users file system, rather than the containers file system. As a result, when running `lhapdf install`, pre-installed PDFs are migrated to `$PWD/.lhapdf`, and further commands use the PDFs contained within.
