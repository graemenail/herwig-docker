#!/bin/bash
source /hep/bin/activate
export PATH="$(find /hep/opt -maxdepth 2 -name 'bin'):$PATH"
export RIVET_ANALYSIS_PATH="$PWD:$RIVET_ANALYSIS_PATH"


# Shim `lhapdf install` call to migrate LHAPDF to the current dir
if  test "$1" = "lhapdf" && test "$2" = "install" && ! test -d "$PWD/.lhapdf"; then
  echo -e "Note: LHAPDF PDF sets currently exist inside this container only."
  mkdir .lhapdf && cp -r /hep/share/LHAPDF/* $PWD/.lhapdf && \
  echo -e "PDF sets have been migrated to your local machine at\n➜ $PWD/.lhapdf"
fi

# Use host LHAPDF data
if [ -d "$PWD/.lhapdf" ]; then
  export LHAPATH="$PWD/.lhapdf"
fi

exec "$@"
