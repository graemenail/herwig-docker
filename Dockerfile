FROM ubuntu:20.04 as base

## Non-Interactive
ENV DEBIAN_FRONTEND=noninteractive

## Install Packages
RUN apt-get update -y \
    && apt-get install -y gcc g++ gfortran \
    && apt-get install -y \
        make automake autoconf libtool cmake \
        git mercurial subversion bzr wget tar less bzip2 findutils nano file rsync \
        zlib1g-dev libgsl-dev \
        graphviz \
    && apt-get clean -y;

## Python Version
ARG PY_VER=3
RUN if test "$PY_VER" = "3"; then \
      # Python 3
      apt-get install -y python3 python3-dev python3-pip \
      && update-alternatives --install /usr/bin/python python /usr/bin/python3 1 \
      && update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 2; \
    else \
      # Python 2
      apt-get install -y python python-dev \
      && update-alternatives --install /usr/bin/python python /usr/bin/python2 1 \
      && wget -O - https://bootstrap.pypa.io/get-pip.py | python -; \
    fi \
    # Packages
    && pip install matplotlib requests Cython \
    && apt-get clean -y;


## Install Latex
ARG LATEX=0
RUN if test "$LATEX" != "0"; then \
      apt-get install -y texlive-latex-recommended texlive-fonts-recommended \
                         texlive-latex-extra texlive-science texlive-pstricks \
                         imagemagick \
      && apt-get clean -y; \
    fi \
    # Enable ImageMagick processing on PS/PDF
    && sed -i 's/domain="coder" rights="none"/domain="coder" rights="read | write"/g' /etc/ImageMagick-6/policy.xml

# Build Herwig
FROM base as builder
WORKDIR /usr/tmp/src
ADD herwig-bootstrap .
RUN ./herwig-bootstrap --src-dir=$PWD -j $(nproc --ignore=1) \
    --without-hjets \
    /hep

## Ensure no tarballs
RUN find /hep -name "*.tar.*" -delete

## RUN commands in HEP env
ENV BASH_ENV=/hep/bin/activate
SHELL ["bash", "-c"]

## Add MadGraph default PDF for systematics
RUN lhapdf install NNPDF23_lo_as_0130_qed

# Deploy
FROM base
COPY --from=builder /hep /hep

## Environment wrapper
ADD env.sh /hep
RUN chmod +x /hep/env.sh

## Image Details
LABEL maintainer="graemenail.work@gmail.com"
LABEL description="Herwig Docker based on Ubuntu 20.04 with Python 2/3 support"
LABEL version="0.6"

ENTRYPOINT ["/hep/env.sh"]
CMD ["Herwig", "--help"]
